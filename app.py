from flask import Flask, request, redirect, render_template

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # Verificar si se ha enviado un archivo
        if 'archivo' not in request.files:
            return redirect(request.url)
        
        archivo = request.files['archivo']
        
        # Verificar si no se ha seleccionado ningún archivo
        if archivo.filename == '':
            return redirect(request.url)
        
        # Guardar el archivo en el sistema de archivos
        archivo.save(archivo.filename)
        
        # Procesar el archivo y realizar las autorizaciones en la base de datos
        
        # Redireccionar a una página de éxito o mostrar un mensaje de éxito
        return 'tu archivo ha sido anclado exitosamente al fondo del oceano'
    
    return render_template('index.html')
    
#INICIO DEL ANCLADO AL OCEANO
if __name__ == '__main__':
    app.run(debug=True)