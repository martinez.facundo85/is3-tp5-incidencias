# Datos del personaje
nombre = "Gandalf"
raza = "Mago"
habilidad = "Manipulación de la magia"
arma = "Báculo"

# Crear el archivo
nombre_archivo = "personaje_fantastico.txt"

with open(nombre_archivo, "w") as archivo:
    archivo.write(f"Nombre: {nombre}\n")
    archivo.write(f"Raza: {raza}\n")
    archivo.write(f"Habilidad: {habilidad}\n")
    archivo.write(f"Arma: {arma}\n")

print("Archivo creado exitosamente.")
